import React, {Component, Fragment} from 'react';
import axios from '../../axios-countries';
import InfoCountries from "../../components/InfoCountries/InfoCountries";
import ItemCountries from "../../components/ItemCountries/ItemCountries";
import './BlogCountries.css';
import Spinner from "../../components/UI/Spinner/Spinner";
import withLoader from "../../hoc/withLoader";

class BlogCountries extends Component {
    state = {
        countries: [],
        countryInfo: {},
    };

    COUNTRY_URL = 'https://restcountries.eu/rest/2/name/';
    ALPHA_URL = 'https://restcountries.eu/rest/v2/alpha/';

    componentDidMount () {
        axios.get()
            .then(countries => {
                this.setState({countries: countries.data})
            }).catch(error => {
            console.log(error);
        })
    }

    postSelectedHandler = name => {
        axios.get(this.COUNTRY_URL + name)
            .then(countries => {
                Promise.all(countries.data[0].borders.map(border => {
                    return axios.get(this.ALPHA_URL + border)
                })).then(response => {
                    let countryInfo = countries.data[0];
                    countryInfo.borders = response.map(country => country.data.name);
                    this.setState({countryInfo})
                })
            }).catch(error => {
            console.log(error);
        })
    };

    render() {

        let countryInfo = <div>
            {Object.keys(this.state.countryInfo).length > 0 ?
                <InfoCountries
                    counytyName={this.state.countryInfo.name}
                    capital={this.state.countryInfo.capital}
                    population={this.state.countryInfo.population}
                    borders={this.state.countryInfo.borders}
                    region={this.state.countryInfo.region}
                    subregion={this.state.countryInfo.subregion}
                    flag={this.state.countryInfo.flag}
                /> : <h2>Here is the information about countries!</h2>

            } </div>;


        if (!this.state.countryInfo) {
            countryInfo = <Spinner />
        }

        return (
            <Fragment>
                <div className='container'>
                    <div className="items">
                        {this.state.countries.map((post, key) =>
                            <ItemCountries
                                key={key}
                                countryName={post.name}
                                clicked={() => this.postSelectedHandler(post.name)}
                            >
                            </ItemCountries>
                        )
                        }
                    </div>
                    {countryInfo}
                </div>
            </Fragment>
        );
    }
}

export default withLoader(BlogCountries, axios);