import React from 'react';
import './ItemCountries.css';

const ItemCountries = props => {
    return (
        <div>
            <div className='item' onClick={props.clicked}>
                <span>{props.countryName}</span>
            </div>
        </div>
    );
};

export default ItemCountries;