import React from 'react';
import './Spinner.css';

const Spinner = props => (
    props.show ? <div className='Spinner' onClick={props.onClick}/> : null
);

export default Spinner;