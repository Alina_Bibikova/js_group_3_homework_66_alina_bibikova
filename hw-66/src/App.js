import React, { Component } from 'react';
import './App.css';
import BlogCountries from "./containers/BlogCountries/BlogCountries";
import {Route, Switch} from "react-router";

class App extends Component {
  render() {
    return (
        <Switch>
            <Route path="/" exact component={BlogCountries} />
        </Switch>
    );
  }
}

export default App;
