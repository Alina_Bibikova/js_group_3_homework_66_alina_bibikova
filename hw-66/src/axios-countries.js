import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://restcountries.eu/rest/v2/all?fields=name'
});

instance.interceptors.request.use(req => {
    console.log('[In request interceptor', req);
    return req;
});


export default instance;